module backend

go 1.17

require github.com/gorilla/mux v1.8.0

require github.com/rs/cors v1.8.2 // indirect

require (
	github.com/felixge/httpsnoop v1.0.2 // indirect
	github.com/gorilla/handlers v1.5.1
)
