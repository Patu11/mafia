package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

type Test struct {
	Id      int    `json:"id"`
	Message string `json:"message"`
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/test", test).Methods("GET")

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000"},
		AllowCredentials: true,
	})
	handler := c.Handler(r)
	fmt.Println("Start listening")
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", handler))
}

func test(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Request")
	w.WriteHeader(http.StatusOK)
	var temp = Test{Id: 1, Message: "Test message"}
	json.NewEncoder(w).Encode(temp)
}
