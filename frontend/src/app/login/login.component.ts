import { Component, OnInit } from '@angular/core';
import { TestService } from '../service/test.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  data: string = 'asd';
  constructor(private testService: TestService) { }

  ngOnInit(): void {
    this.testService.getTest().subscribe(
      data => {
        console.log(data);
        this.data = JSON.stringify(data);
      }
    );
  }

}
