import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  url: string = 'http://localhost:8080/test';

  constructor(private http: HttpClient) { }

  getTest() {
    return this.http.get(this.url);
  }
}
